#!/usr/bin/python2.6
# -*- coding: utf-8 -*
""" This module contains various functions to work with QRadar, mainly to do
some health check """

import sys
import time
import requests

def do_search(host, aqlQuery, token):
	"""Perform an AQL search

	Parameters:
	host(string, mandatory): the IP/FQDN of the qradar console
	aqlQuery (string, mandatory): the aql request to send
	token (string, optional): the authentication token to connect to the API

	Return value:
	The search results (string)"""
	waitingTime = 1
	restEndpoint = 'https://' + str(host) + '/api/ariel/searches'

	print("AQL query:\n" + aqlQuery +'\n')
	sys.stdout.flush()

	# POST the search
	result = requests.post(restEndpoint, verify=False, headers={"SEC":token}, data={'query_expression':aqlQuery}).json()

	# Get the search id
	search_id = result['search_id']

	# Poll to check completion of the search
	p_prev = 0
	time.sleep(waitingTime)
	while True :
		pollResult = requests.get('{0}/{1}'.format(restEndpoint,search_id), verify=False, headers={"SEC":token}).json()
		p_current = pollResult['progress']

		if pollResult['status'] == 'COMPLETED':
			sys.stdout.write('\rProgress: {0: <3}% COMPLETED                         \n'.format(p_current))
			break
		else:
			waitingTime *= (1 - (p_current - p_prev)/100.0 + (100-p_current)*2/100.0)
			p_prev = p_current
			sys.stdout.write('\rProgress: {0: <3}%\tNext check in: {1} seconds'.format(p_current, int(waitingTime)))
			sys.stdout.flush()
			time.sleep(waitingTime)

	# Save the results of the search
	return requests.get(restEndpoint + '/' + search_id + '/results', verify=False, headers={"SEC":token, "Accept":'application/json'}).json()

class ODict:
	"""Ordered dictionnary. Very minimal"""
	def __init__(self):
		self._keys = []
		self._values = []

	def __getitem__(self, k):
		i = self._keys.index(k)
		return self._values[i]

	def __setitem__(self, k, v):
		if k not in self._keys:
			self._keys.append(k)
			self._values.append(v)
		else:
			i = self._keys.index(k)
			self._values[i] = v

	def keys(self):
		return self._keys

	def sort(self):
		""" Bubble sort """
		n = len(self._values) - 1

		while n > 0:
			i = 0
			while i < n:
				if self._values[i] < self._values[i+1]:
					tmpValue = self._values[i]
					tmpKey = self._keys[i]
					self._values[i] = self._values[i+1]
					self._keys[i] = self._keys[i+1]
					self._values[i+1] = tmpValue
					self._keys[i+1] = tmpKey
				i += 1
			n -= 1


def aql_time(t):
	"""Convert seconds since Epoch in a suitable time format for AQL START/STOP clauses

	Parameters:
	t (int, mandatory): the timestamp to convert

	Return value:
	A time string in the format YYYY/MM/dd hh:mm:ss"""
	return time.strftime('%Y/%m/%d %H:%M:%S', time.localtime(t))

def fail(message):
	""" Print error message and exit """
	print(message)
	exit()

def backslash_escape_split(s):
	""" Equivalent to s.split(' '), but ignoring '\'-escaped spaces

	Parameters:
	s (string, mandatory): the string to split

	Return value:
	A list containing words from s, using non backslash-escaped spaces as the separator"""
	ret = []
	tmp = ''
	i = 0

	while i < len(s):
		if s[i] not in ('\\', ' '):
			tmp += s[i]
		elif s[i] == '\\':
			if i < len(s) - 1 and  s[i + 1] == ' ':
				tmp += s[i + 1]
				i += 1
			else:
				tmp += s[i]
		else:
			ret.append(tmp)
			tmp = ''
		i += 1
	ret.append(tmp)
	return ret

def get_refmap_data(host, refmap, token):
	""" Return a dictionnary containing the keys and values in the "data" field of the specified reference map

	Parameters:
	host(string, mandatory): the address of the QRadar console
	refmap(string, mandatory): the name of the reference map
	token (string, mandatory): the authentication token to connect to the API

	Return value:
	Dictionary containing the reference map data """
	refmapsEndpoint = 'https://' + host + '/api/reference_data/maps'
	result = requests.get(refmapsEndpoint + '/' + refmap + '?fields=data', verify=False, headers={"SEC":token}).json()
	return result['data']

def delete_refmap_entry(host, refmap, key, value, token):
	""" Remove an entry from a reference map

	Parameters:
	host(string, mandatory): the address of the QRadar console
	refmap(string, mandatory): the name of the reference map
	key (string, mandatory): the key to remove
	value (key, mandatory): the value to remove
	token (string, mandatory): the authentication token to connect to the API

	Return value:
	Information about the reference map (string)"""

	refmapsEndpoint = 'https://' + host + '/api/reference_data/maps'
	result = requests.request(refmapsEndpoint + '/' + refmap + '/' + key + '?value=' + value, verify=False, headers={"SEC":token}, method='DELETE')
	return result

import cStringIO
HTTP_CODE = 200

def call_curl(url, verifyPeer=True, headers=[], postfields='', customRequest=''):
	"""Call curl with various options and return the resulting string

	Parameters:
	url (string, mandatory): the url to get/post
	verifyPeer (string, optional): whether to check certificate
	headers (list, optional): the list of headers to pass
	postFields (string, optional): POST data to pass
	customRequest (string, optional): custom request to pass to Curl

	Return value:
	The string resulting from the curl call """
	global HTTP_CODE
	c = pycurl.Curl()
	c.setopt(c.URL, str(url))
	if verifyPeer == False:
		c.setopt(c.SSL_VERIFYPEER, 0)
	if headers != []:
		c.setopt(c.HTTPHEADER, headers)
	if postfields != '':
		c.setopt(c.POSTFIELDS, str(postfields))
	if customRequest != '':
		c.setopt(c.CUSTOMREQUEST, str(customRequest))

	buf = cStringIO.StringIO()
	c.setopt(c.WRITEFUNCTION, buf.write)
	c.perform()

	ret = buf.getvalue()
	buf.close()
	HTTP_CODE = c.getinfo(pycurl.HTTP_CODE)

	return ret
