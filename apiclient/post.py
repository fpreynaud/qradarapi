from urllib import quote, urlencode
import requests
import re
from util import handle_search_args, EventSearcher
import data

def create(args = []):
	"""Create the requested element: (e.g reference map, AQL search...)

	Parameters:
	args(list, mandatory): the type of element to create + the arguments"""
	if args == []:
		print('Create what ?')
		return
	else:
		toCreate = args[0]
		nargs = len(args[1:])


	if toCreate == 'refmap':
		if nargs < 1:
			print('Missing argument(s)! Usage: create refmap <name> [ALN|NUM|IP|PORT|ALNIC|DATE]')
			return
		elif nargs < 2:
			elementType = 'ALN'
		else:
			elementType = args[2]

		name = args[1]
		requests.post(url=data.refmapsRoot, verify=False, headers=data.defaultHeaders, data={'element_type':elementType,'name':name})

	elif toCreate == 'search':
		if nargs < 1:
			print('Missing AQL query! Usage: create search <AQL query>')
			return

		aqlQuery = ' '.join(args[1:])
		query = urlencode({'query_expression':aqlQuery})
		#query = {'query_expression':'='.join(query.split('=')[1:])}
		query = {'query_expression':aqlQuery}
		result = requests.post(url=data.searchesRoot, verify=False, headers=data.defaultHeaders, data=query).text

		# Create thread to poll completion, pre-notify, and write the results

		search_idRegex = r'search_id":\s*"([-a-f0-9]+?)"'
		progressRegex = r'progress":\s*([^,]+),'
		statusRegex = r'status":\s*"([^"]+)",'
		match = re.search(search_idRegex, result)
		match2 = re.search(progressRegex, result)
		match3 = re.search(statusRegex, result)
		try:
			search_id = match.group(1)
			searchProgress = match2.group(1)
			searchStatus = match3.group(1)
		except:
			print(result)
			return
		alias = 'sch{0}'.format(data.searchNumber)
		print('Created search {0}. Alias: {1}'.format(search_id, alias))
		t = EventSearcher(search_id, alias)
		t.start()
		data.threads.append(t)
		data.searches[alias] = {'id':search_id,'req':aqlQuery, 'status':searchStatus, 'progress':searchProgress, 'notified':False}
		data.searchNumber += 1
	else:
		print("Invalid argument {0}".format(toCreate))

def update(args):
	"""Update the requested element: (e.g reference map, search...)

	Parameters:
	args(list,mandatory): the type of element to update + the arguments"""
	element = args[0]
	nargs = len(args[1:])

	# Update reference map
	if element == 'refmap':
		if nargs < 3:
			print('Usage: update refmap <refmap_name> <key> <value>')
			return
		name, key, value = quote(args[1]), quote(args[2]), quote(args[3])
		requests.post(url=data.refmapsRoot + '/{0}'.format(name), verify=False, headers=data.defaultHeaders, data={'key':key,'value':value,'source':'apiclient'})

	# Cancel search
	elif element == 'search':
		if nargs == 0:
			print('Usage: update search <search_id>')
			return

		search_id = handle_search_args(args[1:])
		if search_id is None:
			return

		requests.post(data.searchEndpoint.format(searchId=search_id), verify=False, headers=data.defaultHeaders, data={'status':'CANCELED'})

	else:
		print("Invalid argument {0}".format(element))
		return None

