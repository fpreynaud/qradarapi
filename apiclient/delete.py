import json
import data
import requests
from util import handle_search_args

def delete(args):
	"""Delete the requested data: (e.g reference map)

	Parameters:
	args(list,mandatory): the type of data to delete + the arguments"""
	element = args[0]
	nargs = len(args[1:])
	if element == 'refmap':
		if nargs == 0:
			print('Usage: delete refmap <refmap name> [key]')
			return
		name = args[1]
		if nargs >= 2:
			key = args[2]
			#get the value
			value = requests.get(data.refmapEndpoint.format(name) + '?fields=data', verify=False, headers=data.defaultHeaders)
			value = json.loads(value)['data'][key]['value']
			#inject it in deletion request
			print(requests.request(url=data.refmapEndpoint.format(name) + '/{0}?value={1}'.format(key,value),  verify=False, headers=data.defaultHeaders, method='DELETE'))
			return
		# No key given, delete entire refmap
		requests.request(url=data.refmapEndpoint.format(name), verify=False, headers=data.defaultHeaders, method='DELETE')
	elif element == 'search':
		search_id = handle_search_args(args[1:])
		if search_id is None:
			return

		requests.request(url=data.searchEndpoint.format(searchId=search_id), verify=False, headers=data.defaultHeaders, method='DELETE')
	else:
		print("Invalid argument {0}".format(element))
		return None

