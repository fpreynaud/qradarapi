import data
import requests
import os
import time
from threading import Thread
import json

class Notification:
	def __init__(self, severity, message):
		self.severity = severity
		self.message = message

	def __repr__(self):
		colors = {'ERROR':'\033[31;1m', 'WARNING':'\033[33;1m', 'INFO':'\033[32;1m'}
		return '{0}{1}\033[0m: {2}'.format(colors[self.severity], self.severity, self.message)

# Class to poll search completion and write results on disk
class EventSearcher(Thread):
	def __init__(self, search_id, alias):
		Thread.__init__(self)
		self.search_id = search_id
		self.alias = alias

	def run(self):
		waitingTime = 3

		while data.running :
			try:
				response = json.loads(requests.get(url=data.searchEndpoint.format(searchId=self.search_id), verify=False, headers=data.defaultHeaders).text)
				status = response['status']
				progress = response['progress']
				data.searches[self.alias]['status'] = status
				data.searches[self.alias]['progress'] = progress
			except KeyError:
				with data.lock:
					data.notifications.append(Notification('ERROR','Could not get status for {0}.'.format(self.search_id)))
					break

			if status == 'COMPLETED':
				with data.lock:
					data.notifications.append(Notification('INFO', 'Search {sid} completed.'.format(sid=self.search_id)))
				break
			else:
				time.sleep(waitingTime)
				if waitingTime < 60:
					waitingTime *= 2
				if waitingTime > 60:
					waitingTime = 60

		outputDir = '{0}/search_results'.format(data.home)
		os.system('mkdir -p {0}'.format(outputDir))
		name = '{0}/{1}.csv'.format(outputDir,self.search_id)
		headers = data.defaultHeaders.copy()
		headers.update({'Accept':'application/csv'})
		results = requests.get(url=data.resultsEndpoint.format(searchId=self.search_id), verify=False, headers=headers).text

		with open(name, 'w') as output:
			output.write(results)
			with data.lock:
				data.notifications.append(Notification('INFO', 'Results for {sid} written to {out}'.format(sid=self.search_id, out=name)))

def handle_search_args(args):
	if args == []:
	# If no search id, or search alias is provided, use the one from the latest search created
		if data.searches.keys() == []:
			print('Missing argument(s). You need to provide a search_id or an existing alias')
			return None
		else:
			search_id = data.searches[data.searches.keys()[-1]]['id']
	else:
		search_id = args[0]
		if search_id in data.searches.keys(): # If it is an alias that is provided
			search_id = data.searches[search_id]['id']

	return search_id

# Basically str.split with backslash escaping
def get_words(s,seps = [' ', '\t', '\n']):
	""" @desc Constructs a list a strings from a string, according to a list of word separators. Backslash escape supported
	@param s (string, mandatory): the string the list will be derived from
	@param seps (list of strings, optional): the list of word separators
	@return list of strings corresponding to the words in s. """
	tmp = ''
	besc = False
	words = []

	for c in s:
		if c == '\\':
			besc = True
			continue
		elif c in seps:
			if besc:
				tmp += c
				besc = False
			else:
				words.append(tmp)
				tmp = ''
		else:
			tmp += c
			besc = False

	words.append(tmp)

	return words
