import qradarlib as hc
import os
from os.path import isfile
from threading import RLock
searches = hc.ODict()
console = '81.255.244.30'
token = '2120d6dd-d1b4-48b0-a890-4ed3daf2dc69'
apiRoot = 'https://{0}/api'.format(console)
searchesRoot = '{0}/ariel/searches'.format(apiRoot)
defaultHeaders = {'SEC':token, 'Accept':'application/json'}
searchEndpoint = '{0}/{{searchId}}'.format(searchesRoot)
resultsEndpoint = '{0}/results'.format(searchEndpoint)
refmapsRoot = '{0}/reference_data/maps'.format(apiRoot)
searchNumber = 1
running = True
notifications = []
threads = []
lock = RLock()
home = os.popen('echo -n $HOME').read()
refmapEndpoint = '{0}/{{0}}'.format(refmapsRoot)
mapsofsetsRoot = '{0}/reference_data/map_of_sets'.format(apiRoot)

def load_conf(configFile):
	global console
	global token
	if isfile(configFile):
		with open(configFile, 'r') as conf:
			config = {}
			for line in conf.readlines():
				l = line.split('=')
				config[l[0]] = l[1].rstrip()
		console = config['console']
		token = config['token']
