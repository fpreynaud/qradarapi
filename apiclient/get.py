import data
import json
from util import handle_search_args

def display(args):
	"""Print the requested element: (e.g list of reference maps, search results...)

	Parameters:
	args(list, mandatory): the type of element to print + the arguments"""

	if args == []:
		print('Print what ?')
		return
	else:
		printable = args[0]
		nargs = len(args[1:])

	if printable == 'mapsofsets':
		results = requests.get(url=data.mapsofsetsRoot, verify=False, headers=data.defaultHeaders)
		mapsofsetsList = json.loads(results)
		print('List of reference maps of sets (total: {0}):\n'.format(len(mapsofsetsList)))
		for mos in mapsofsetsList:
			print('- {0}'.format(mos['name']))
	elif printable == 'refmapofsets':
		if nargs < 1:
			print('Reference map of sets name required!')
			return

		refmapOfSets = '%20'.join(args[1:])
		results = requests.get(url='{0}/{1}?fields=data'.format(data.mapsofsetsRoot,refmapOfSets), verify=False, headers=data.defaultHeaders + ['Range: items=0-9'])
		refdata = json.loads(results)
		for k,v in refdata['data'].items():
			print('{0}:'.format(k))
			for elt in v:
				print('\t- {0}'.format(elt['value']))
	elif printable == 'refmaps':
		results = requests.get(url='https://{0}/api/reference_data/maps?fields=name'.format(data.console), verify=False, headers=data.defaultHeaders)
		refmapList = json.loads(results)
		print('List of reference maps (total: {0}):\n'.format(len(refmapList)))
		for refmap in refmapList:
			print('- {0}'.format(refmap['name']))
	elif printable == 'searchresults':
		search_id = handle_search_args(args[1:])
		if search_id is None:
			return

		results = requests.get(url=data.resultsEndpoint.format(searchId=search_id), verify=False, headers=data.defaultHeaders + ['Accept: application/csv', 'Range: items=0-99'])

		print('Printing the first 100 results for {0}:\n'.format(search_id))
		for l in results.split('\n')[:100]:
			print(l)

	elif printable == 'searchstatus':
		search_id = handle_search_args(args[1:])
		if search_id is None:
			return

		try:
			results = requests.get(url=data.searchEndpoint.format(searchId=search_id), verify=False, headers=data.defaultHeaders)
			response = json.loads(results)
			status = response['status']
			progress = response['progress']
			print('Status: {0}\nProgress: {1}%'.format(status, progress))
		except KeyError:
			print('Error: Could not get status for {0}.'.format(search_id))

	elif printable == 'searchinfo':
		for alias in data.searches.keys():
			print('{name}:\n'
				'\tQuery  : {request}\n'
				'\tStatus   : {status}\n'
				'\tProgress : {progress}'.format(name=alias,request=data.searches[alias]['req'],status=data.searches[alias]['status'], progress=data.searches[alias]['progress'])) 

	elif printable == 'uncompletedsearches':
		print('Getting the list of uncompleted searches. This might take some time')

		if nargs == 2:
			headers = data.defaultHeaders + ['Range: items={low}-{high}'.format(low=args[1],high=args[2])]
		else:
			headers = data.defaultHeaders

		results = requests.get(url=data.searchesRoot, verify=False, headers=headers)
		searchIdList = json.loads(results)
		searches = {}

		for search_id in searchIdList:
			results = requests.get(url=data.searchEndpoint.format(searchId=search_id), verify=False, headers=defaultHeaders)
			api_response = json.loads(results)

			try:
				status = api_response['status']
			except KeyError:
				with open('apiclient.log', 'a') as f:
					f.write('{0}\n'.format(api_response))

			if status != 'EXECUTE':
				continue

			query = api_response['query_string']
			searches[search_id] = {'status':status, 'query':query}

		for search in searches:
			print(search, searches[search]['status'], searches[search]['query'])

	elif printable == 'refmap':
		if nargs < 1:
			print('Reference map name required!')
			return

		refmap = '%20'.join(args[1:])
		results = requests.get(url='{0}/{1}?fields=data'.format(data.refmapsRoot,refmap), verify=False, headers=data.defaultHeaders)
		refdata = json.loads(results).get('data', {})

		for key,value in refdata.items():
			print('{0}    {1}'.format(key,value['value']))
	else:
		print("Invalid argument {0}".format(printable))

def print_help(command = []):
	""" Display the help for the requested command

	Parameters:
	command(list, optional): the command (and potentially sub-command) to get help on"""

	if command == []:
		print("Console: {0}\n\n"
		"Commands:\n"
		"print - Print an element\n"
		"create - Create an element\n"
		"update - Update an element\n"
		"delete - Delete an element\n"
		"For more details on a command, type help <command>".format(data.console))
	else:
		action = command[0]
		if action == 'print':
			print('print refmaps: print the list of all refmaps\n'
			'print refmap <refmap name>: print the content of a specific refmap\n'
			'print mapsofsets: print the list of all reference maps of sets\n'
			'print refmapofsets <map of sets name>: print the content of a specific reference map of sets\n'
			'print searchresults <search_id>|<alias>: print the results for a specific search\n'
			'print searchstatus <search_id>|<alias>: print the status for a specific search\n'
			'print searchinfo: print information about all searches')
		elif action == 'create':
			print('create refmap <name> [ALN|NUM|IP|PORT|ALNIC|DATE]: create a reference map\n'
			'create search <AQL query>: create an Ariel search')
		elif action == 'update':
			print('update refmap <name> <key> <value>: add <key>,<value> pair to refmap <name> or update <key> with <value> in refmap <name>\n'
			'update search <search_id>: cancel specific search')
		elif action == 'delete':
			print('delete refmap <name> [key]: delete an entire refmap or only a specific value in a refmap\n'
			'delete search <search_id>: delete specific search')
