#!/usr/bin/python
# -*- coding: utf-8 -*

import argparse
import sys
import get
import post
import delete
import data
from util import get_words

parser = argparse.ArgumentParser()
parser.add_argument('-f', default='apiclient.conf', type=str)
args = parser.parse_args()
data.load_conf(args.f)

def exitClient():
	print('Exiting...')
	data.running = False

def execute(name, *args, **kwargs):
	callback = {
		"help": get.print_help,
		"exit": exitClient,
		"print": get.display,
		"create": post.create,
		"update": post.update,
		"delete": delete.delete
	}

	func = callback.get(name, lambda:"{prog}: {act}: command not found".format(prog=sys.argv[0], act=name))
	return func(*args, **kwargs)

def notify():
	""" Print latest notifications """
	while data.notifications:
		print(data.notifications.pop(0))

def main():
	print(' QRadar REST API client v1.0 '.center(80, '#'))
	execute('help')
	while data.running:
		notify()
		try:
			command = get_words(raw_input('> '))
		except KeyboardInterrupt:
			sys.stdout.write('\n')
			continue
		except EOFError:
			exitClient()
			continue

		if command == ['']:
			continue
		action = command[0]
		
		if command[1:] == []:
			execute(action)
		else:
			execute(action, command[1:])

if __name__ == '__main__':
	main()
