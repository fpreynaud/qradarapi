import requests
import json
from fancy_logging import *

def error(message):
	print('\x1b[1;31m[-] {0}\x1b[0m'.format(message))

def success(message):
	log(message,32)

class APIClient:
	def __init__(self, host, token):
		self.host = host
		self.token = token
		self.headers = {'SEC':token}

	def request(self, endpoint, method = 'GET', headers={},params={},data={},json=None):
		headers.update(self.headers)
		return requests.request(method=method, url = 'https://{0}/api/{1}'.format(self.host, endpoint), headers = headers, data = data, json = json, verify=False, params=params)

	def get(self, endpoint, params={}, headers={}):
		return self.request(endpoint, params=params, headers=headers)

	def post(self, endpoint, **kwargs):
		return self.request(endpoint, method = 'POST', data=kwargs)


	def list_reference_maps(self):
		try:
			resp = self.get('/reference_data/maps')
		except Exception,e:
			error('Error while querying the list of reference maps')
			print(e)
		return resp.json()

	def search_reference_map(self, refmap):
		ret =  []
		try:
			resp = self.get('/reference_data/maps',params = {'filter':'name={0}'.format(refmap)})
			ret = resp.json()
		except Exception,e:
			error('Error while querying the list of reference maps')
			print(e)
		return ret != []

	def create_reference_map(self, name, elementType, keyLabel, valueLabel):
		try:
			resp = self.post('/reference_data/maps', name = name, element_type = elementType, key_label = keyLabel, value_label = valueLabel)
		except Exception,e:
			error('Error while creating the reference map')
			print(e)
		return resp

	def update_reference_map(self, name, data):
		try:
			resp = self.request('reference_data/maps/bulk_load/{0}'.format(name),method='POST',json=data, headers = {'Content-Type':'application/json'})
		except Exception,e:
			error('Error while updating the reference map')
			print(e)
		return resp
	
	def delete_reference_map(self,name, purge = 'false'):
		try:
			resp = self.request('reference_data/maps/{0}'.format(name),method='DELETE', headers = {'Content-Type':'application/json'}, data = {'purge_only':purge})
		except Exception,e:
			error('Error while deleting/purging the reference map')
			print(e)
		return resp

	def purge_reference_map(self, name):
		return self.delete_reference_map(name, 'true')


	def display_reference_map(self, name, limit=(0,50)):
		if limit is not None:
			headers = {'Range':'items={0}-{1}'.format(limit[0],limit[1])}
			headers.update(self.headers)
			resp = self.get('/reference_data/maps/' + name, headers=headers)
		else:
			resp = self.get('/reference_data/maps/' + name)
		return resp.json()

def http_error(response):
	ret = False
	if response.status_code >= 400:
		error('Received HTTP {0}'.format(response.status_code))
		ret = True
	else:
		success('Received HTTP {0}'.format(response.status_code))
	return ret

