def log(message, colorCode=34):
	print('\x1b[1;{1}m[+] {0}\x1b[0m'.format(message, colorCode))

def error(message):
	print('\x1b[1;31m[-] {0}\x1b[0m'.format(message))

def success(message):
	log(message,32)

def list_overview(l, limit=30):
	for item in l[:limit] if limit > 0 else l:
		print(item)

	if len(l) > limit and limit > 0:
		print('...(Truncated to first {0} items)'.format(limit))

def dict_overview(d, limit=30):
	for key in d.keys()[:limit]:
		print('{0:.<50} -> {1}'.format(key[:50],d[key]))

	if len(d.keys()) > limit:
		print('...(Truncated to first {0} items)'.format(limit))

